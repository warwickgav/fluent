

	var accordionContent = $('.js-accordionWrap');

	accordionContent.hide();

	$('.js-accordionTitle').click(function () {
		if ($(this).parent().hasClass('is-open')) {
			$(this).parent().removeClass('is-open');
			$(this).next().slideUp();
		} else {
			accordionContent.slideUp();
			$('.electorate-details > .election-group ').removeClass('is-open');
			$(this).parent().addClass('is-open');
			$(this).next().slideDown();
		}
	});



