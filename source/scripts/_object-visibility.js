// Detect when an element is fully visible in the viewport
// This is a very basic version

$.fn.isFullyInViewport = function() {
	var elementTop = $(this).offset().top;
	var elementBottom = elementTop + $(this).outerHeight();

	var viewportTop = $(window).scrollTop();
	var viewportBottom = viewportTop + $(window).height();

	return elementTop >= viewportTop && elementBottom <= viewportBottom;
};

$(window).on('resize scroll', function() {

	// Check each object that uses 'is-invisible'
	$('.is-invisible').each(function() {
		var visibleObject = $(this).attr('id');
		if ($(this).isFullyInViewport()) {
			// Add a class to make it visible
			$(visibleObject).addClass('is-visible');
		} else {
			// Remove class to make it invisible
			$(visibleObject).removeClass('is-visible');
		}
	});
});