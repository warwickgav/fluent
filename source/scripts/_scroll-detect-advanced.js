

	// This script detects when a fixed header has scrolled past a certain point
	// It adds a class allowing you to adjust the header, for example; making it thinner
	// When you scroll up it removes the class

	var scrolling;
	var lastScrollTop = 0;
	var delta = 1;	// The point of no return
	var headerHeight = $('.site__header').outerHeight(); // Find the height of the header

	$(window).scroll(function(event){
		scrolling = true;
	});

	setInterval(function() {
		if (scrolling) {
			hasScrolled();
			didScroll = false;
		}
	}, 250);

	function hasScrolled() {
		var st = $(this).scrollTop();

		if(Math.abs(lastScrollTop - st) <= delta) return; // Make sure they scroll more than delta

		if (st > lastScrollTop && st > headerHeight){
			// Scroll Down
			$('body').removeClass('scrolling-up').addClass('scrolling-down');
		} else {
			// Scroll Up
			if(st + $(window).height() < $(document).height()) {
				$('body').removeClass('scrolling-down').addClass('scrolling-up');
			}
		}

		lastScrollTop = st;
	}

	$(window).scroll(function(event){
		scrolling = true;
	});

