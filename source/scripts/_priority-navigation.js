

	// This script detects when a fixed header has scrolled past a certain point
	// It adds a class allowing you to adjust the header, for example; making it thinner
	// When you scroll up it removes the class

	var nav = $('.navigation__main > ol'),
		navItems = $('.navigation__main > ol > li'),
		moreNav = $('.more-dropdown'),
		navAllItems = 0,
		secondaryNavHTML = [];

	navItems.each(function(){
		$(this).attr('data-width', $(this).width());
		navAllItems += $(this).width();
	});

	navAllItems = navAllItems + (navItems.length * 4);

	if (nav.width() < navAllItems) {
		$('.navigation__main .secondary').hide();

		$('.navigation__main > ol .secondary').each(function(){
			secondaryNavHTML.push('<li>' + $(this).html() + '</li>');
		});

		$('.more-dropdown_menu').html(secondaryNavHTML);

		moreNav.css('display','inline-block');

	} else {
		$('.navigation__main .secondary').show();

		$('.more-dropdown_menu').empty();

		moreNav.hide();
	}

