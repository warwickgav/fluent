

	// This script detects when someone has scrolled and adds a class.
	// It removes the class when you get back to the top

	var scrolledDistanceFromTop = 80; // Detect how far you have scrolled from the top

	$(window).scroll(function () {
		var scrollPosition = $(this).scrollTop();
		if (scrollPosition >= scrolledDistanceFromTop ) {
			$('body').addClass('has-scrolled');
		} else {
			$('body').removeClass('has-scrolled');
		}
	});

